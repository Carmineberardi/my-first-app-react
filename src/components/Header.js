import { NavLink } from "react-router-dom";

export const Header = () =>{
return(
    <nav className="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <NavLink activeClassName ='active' className="navbar-brand" to="/">Mytodo</NavLink>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink activeClassName ='active' className="nav-link active" aria-current="page" to ="/">Home</NavLink>
              </li>
              <li className="nav-item">
                <NavLink activeClassName ='active' className="nav-link" to="/lists">Lists</NavLink>
              </li>
              <li className="nav-item">
                <NavLink activeClassName ='active' className="nav-link" to="/todos">Todos</NavLink>
              </li>
              <li className="nav-item">
                <NavLink activeClassName ='active' className="nav-link "to="/login">Login</NavLink>
              </li>
              <li className="nav-item">
                <NavLink activeClassName ='active' className="nav-link "to="/register">Register</NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }

export default Header;