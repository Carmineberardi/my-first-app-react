import React, { useEffect, useRef } from 'react';
import { useParams,useLocation,useMatch } from 'react-router-dom';
import {useNavigate} from 'react-router-dom';
import { toast } from 'react-toastify';
import Edit from '../../components/AddElement'
import { useUpdateListMutation } from '../../service/listsService';
const EditList = () => {
    const navigate = useNavigate();
    const [updateList, { isSuccess, error, isError }] = useUpdateListMutation();
    const listEl = useRef('');

    let { list_id } = useParams();
    list_id = Number(list_id);
    const { search } = useLocation();
const pulp = new URLSearchParams(search);
let list_name;
if(pulp)
{
 list_name = pulp.get('list_name') ?? '';
}
    const manageClick = (evt) =>{
        evt.preventDefault();
updateList ({name : listEl.current.value, id : list_id});
    }
    useEffect(() =>{
        if(list_name){
            listEl.current.value = list_name;
        }
        if(isSuccess){
            navigate('/lists')
        }
        if(error){
            toast.error(error.error)
        }
        return() =>{

        }
    },[isSuccess,error])
  return( <div>
      <h1>Edit List</h1>
      <Edit Ele= {listEl} txtButton={'EditList'} manageClick = {manageClick}/>
  </div>);
};
export default EditList;
