export const APIURL = 'http://127.0.0.1:3004';

export const TODO_URL = APIURL + '/todos';
export const LISTS_URL = APIURL + '/lists';
export const FILTER_URL = APIURL + '/filter';
