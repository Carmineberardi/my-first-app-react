import React, {
   useEffect
} from 'react';

import { ToastContainer} from 'react-toastify';
import { BrowserRouter as Router,Routes, Route } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import Mytodos from './features/todos/Mytodos';

import './App.css';

import Header from './components/Header';

import Lists from './features/lists/Lists';
import EditList from '../src/features/lists/EditList';

function App() {
  
  
   useEffect(() => {
    
    //dispatch(getTodos())
    //.unwrap()
    //.then((res)=>{})
    //.catch((error) =>{
      //toast.error(error.message)
    //});

    return () => {
     
    }
  }, [])
  return (
    <div className="App container-fluid">
      <Router>
        <div className="row d-flex justify-content-center">
          <Header/>
         <Routes>
           <Route 
               path='/todos' element={<Mytodos/>}>
                  </Route>
                    <Route path='/lists/:list_id/todos'   element={<Mytodos />}>
                          </Route>
                        <Route path='/lists/:list_id/edit' element ={<EditList />}>
                            
                        </Route>
                        <Route exact path='/lists' element ={<Lists/>}>
                            </Route>
          </Routes>
          </div>
        </Router>
      
     <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
/>
    </div>
  );
}


export default App;
